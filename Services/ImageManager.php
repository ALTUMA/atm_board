<?php

namespace ATM\BoardBundle\Services;

use \Imagick;

class ImageManager{

    private $rootDir;
    private $config;

    public function __construct($kernel_rootdir,$atm_board_config)
    {
        $this->rootDir = $kernel_rootdir;
        $this->config = $atm_board_config;
    }

    public function cropImage($sourceImage,$imageName,$imageDestination,$putWatermark = true){
        $tmpFolder = $this->rootDir.'/../web/'.$this->config['media_folder'].'/temp';
        if(!is_dir($tmpFolder)){
            mkdir($tmpFolder);
        }

        $configWidth = $this->config['image_width'];
        $configHeigh = $this->config['image_height'];
        $imagick = new Imagick($sourceImage);
        if($imagick->getImageFormat() != 'GIF'){
            $imagick->cropThumbnailImage($configWidth,$configHeigh);
            $imagick->writeImage($imageDestination.'/'.$imageName);
            if($putWatermark){
                return $this->watermarkImage($imageDestination.'/'.$imageName,$imageDestination.'/'.$imageName);
            }else{
                return $imageDestination.'/'.$imageName;
            }

        }else{

            $imagick = $imagick->coalesceImages();
            do{
                $imagick->thumbnailImage($configWidth, $configHeigh);
                $imagick->setImagePage($configWidth, $configHeigh, 0, 0);

                if($putWatermark){
                    $frameSize = $imagick->getImageGeometry();

                    if($frameSize['width'] == $this->config['image_width'] && $frameSize['height'] == $this->config['image_height']){
                        $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_small']);
                        $margin = 5;
                    }elseif($frameSize['width'] > 1500){
                        $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_big']);
                        $margin = 10;
                    }else{
                        $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_medium']);
                        $margin = 5;
                    }

                    $watermarkSize = $im_watermark->getImageGeometry();
                    $coordinateX = ($frameSize['width'] - $watermarkSize['width']) - $margin;
                    $coordinateY = ($frameSize['height'] - $watermarkSize['height']) - $margin;

                    $imagick->compositeImage($im_watermark,Imagick::COMPOSITE_OVER, $coordinateX,$coordinateY);
                }
            }while($imagick->nextImage());

            $imagick = $imagick->deconstructImages();
            $imagick->writeImages($imageDestination.'/'.$imageName,true);
            return $imageDestination.'/'.$imageName;
        }

    }

    public function watermarkImage($original_file, $destination_file)
    {
        $im = new Imagick($original_file);

        if($im->getImageFormat() != 'GIF'){
            $imageSize = $im->getImageGeometry();
            if($imageSize['width'] == $this->config['image_width'] && $imageSize['height'] == $this->config['image_height']){
                $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_small']);
                $margin = 5;
            }elseif($imageSize['width'] > 1500){
                $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_big']);
                $margin = 10;
            }else{
                $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_medium']);
                $margin = 5;
            }

            $watermarkSize = $im_watermark->getImageGeometry();
            $coordinateX = ($imageSize['width'] - $watermarkSize['width']) - $margin;
            $coordinateY = ($imageSize['height'] - $watermarkSize['height']) - $margin;

            $im->compositeImage($im_watermark,Imagick::COMPOSITE_OVER, $coordinateX,$coordinateY);
            $im->writeImage($destination_file);
        }else{

            $im = $im->coalesceImages();
            do{
                $frameSize = $im->getImageGeometry();

                if($frameSize['width'] == $this->config['image_width'] && $frameSize['height'] == $this->config['image_height']){
                    $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_small']);
                    $margin = 5;
                }elseif($frameSize['width'] > 1500){
                    $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_big']);
                    $margin = 10;
                }else{
                    $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_medium']);
                    $margin = 5;
                }

                $watermarkSize = $im_watermark->getImageGeometry();
                $coordinateX = ($frameSize['width'] - $watermarkSize['width']) - $margin;
                $coordinateY = ($frameSize['height'] - $watermarkSize['height']) - $margin;

                $im->compositeImage($im_watermark,Imagick::COMPOSITE_OVER, $coordinateX,$coordinateY);
            }while($im->nextImage());
           

            $im->deconstructImages();
            $im->writeImages($destination_file,true);
        }


        return $destination_file;
    }
}