<?php

namespace ATM\BoardBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use Knp\Component\Pager\PaginatorInterface;
use ATM\BoardBundle\Entity\Post;

class SearchPosts{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options)
    {
        $defaultOptions = array(
            'board_id' => null,
            'date_range' => array(
                'init_date' => null,
                'end_date' => null
            ),
            'content_type' => null,
            'order_by_field' => 'creation_date',
            'order_by_direction' => 'DESC',
            'only_with_links' => null,
            'ids' => null,
            'show_pending_videos' => false,
            'pagination' => null,
            'max_results' => null,
            'page' => 1
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();

        $qbIds
            ->select('p.id')
            ->from('ATMBoardBundle:Post', 'p');


        if (!is_null($options['board_id'])) {
            $qbIds
                ->join('p.board', 'b', 'WITH', $qbIds->expr()->eq('b.id', $options['board_id']));
        }

        if (!is_null($options['date_range']['init_date'])) {
            $qbIds->andWhere($qbIds->expr()->gte('p.creation_date', $qbIds->expr()->literal($options['date_range']['init_date'])));
        }

        if (!is_null($options['date_range']['end_date'])) {
            $qbIds->andWhere($qbIds->expr()->lte('p.creation_date', $qbIds->expr()->literal($options['date_range']['end_date'])));
        }

        if (!is_null($options['ids'])) {
            $qbIds->andWhere($qbIds->expr()->in('p.id', $options['ids']));
        }


        if(in_array($options['content_type'],array('videos','all',null))){

            if(!$options['show_pending_videos']){
                $qbIds->leftJoin('p.video', 'v')
                    ->andWhere(
                        $qbIds->expr()->orX(
                            $qbIds->expr()->isNull('v.id'),
                            $qbIds->expr()->andX(
                                $qbIds->expr()->isNotNull('v.id'),
                                $qbIds->expr()->eq('v.isEncoded',1)
                            )
                        )
                    )
                ;
            }else{
                $qbIds->leftJoin('p.video', 'v');
            }
        }


        if(!is_null($options['content_type'])){
            switch($options['content_type']){
                case 'images':
                    $qbIds->join('p.images','i');
                    break;
                case 'videos':
                    $qbIds->join('p.video','v');
                    break;
                case 'text':
                    $qbIds
                        ->leftJoin('p.images','i')
                        ->leftJoin('p.video','v')
                        ->andWhere(
                            $qbIds->expr()->andX(
                                $qbIds->expr()->isNull('i.id'),
                                $qbIds->expr()->isNull('v.id')
                            )
                        );
                    break;
            }
        }

        if(!is_null($options['only_with_links'])){
            $qbIds->join('p.post_to_link','pl');
        }

        $qbIds->orderBy('p.' . $options['order_by_field'], $options['order_by_direction']);

        $query = $qbIds->getQuery();

        $resultCache_id = Post::RESULT_CACHE_COLLECTION_PREFIX.$options['board_id'].md5($query->getSQL());
        $resultCache_ttl = Post::RESULT_CACHE_COLLECTION_TTL;

        $query
            ->useQueryCache(true)
            ->setResultCacheLifetime($resultCache_ttl)
            ->setResultCacheId($resultCache_id);

        $pagination = null;
        if (!is_null($options['pagination'])) {
            $arrIds = array_map(function ($p) {
                return $p['id'];
            }, $qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        } else {
            $queryIds = $qbIds->getQuery();
            if (!is_null($options['max_results'])) {
                $queryIds->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $queryIds->getArrayResult());
        }

        $results = array();
        if(count($ids) > 0){
            foreach($ids as $id){

                $qb = $this->em->createQueryBuilder();
                $qb
                    ->select('post')
                    ->addSelect('board')
                    ->addSelect('user')
                    ->addSelect('images')
                    ->addSelect('video')
                    ->addSelect('pl')
                    ->from('ATMBoardBundle:Post', 'post')
                    ->join('post.board', 'board')
                    ->join('board.user','user')
                    ->leftJoin('post.images', 'images')
                    ->leftJoin('post.video', 'video')
                    ->leftJoin('post.post_to_link','pl')
                    ->where($qb->expr()->eq('post.id', $id));

                $query = $qb->getQuery();
                $query
                    ->useQueryCache(true)
                    ->setResultCacheLifetime(Post::RESULT_CACHE_ITEM_TTL)
                    ->setResultCacheId(Post::RESULT_CACHE_ITEM_PREFIX.$id);

                $post = $query->getArrayResult();
                $results[] = $post[0];
            }
        }

        return array(
            'results' => $results,
            'pagination' => $pagination
        );
    }
}
