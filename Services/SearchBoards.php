<?php

namespace ATM\BoardBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use ATM\BoardBundle\Entity\Board;

class SearchBoards{

    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options){

        $defaultOptions = array(
            'date_range' => array(
                'init_date' => null,
                'end_date' => null
            ),
            'board_ids_not' => null,
            'sorting' => 'recent_activity',
            'ids' => null,
            'pagination' => null,
            'max_results' => null,
            'page' => 1
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();

        $qbIds
            ->select('b.id')
            ->from('ATMBoardBundle:Board','b');

        if (!is_null($options['board_ids_not'])) {
            $qbIds
                ->andWhere($qbIds->expr()->notIn('b.id', $options['board_ids_not']));
        }


        if (!is_null($options['date_range']['init_date'])) {
            $qbIds->andWhere($qbIds->expr()->gte('b.creation_date', $qbIds->expr()->literal($options['date_range']['init_date'])));
        }

        if (!is_null($options['date_range']['end_date'])) {
            $qbIds->andWhere($qbIds->expr()->lte('DATE(b.creation_date)', $qbIds->expr()->literal($options['date_range']['end_date'])));
        }

        switch($options['sorting']){
            case 'recent_activity':
                $qbLatestContent = $this->em->createQueryBuilder();
                $qbLatestContent->select(
                    $qbLatestContent->expr()->max('_p.creation_date'))
                    ->from('ATMBoardBundle:Post', '_p')
                    ->join('_p.board', '_p_b')
                    ->where(
                        $qbLatestContent->expr()->andX(
                            $qbLatestContent->expr()->eq('_p_b.id', 'b.id'),
                            $qbLatestContent->expr()->lte('DATE(_p_b.creation_date)', $qbLatestContent->expr()->literal(date('y-m-d')))
                        )
                    );
                $qbIds
                    ->addSelect('('.
                        $qbLatestContent->getQuery()->getDQL()
                        .') AS HIDDEN recent_activity')
                    ->orderBy('recent_activity','DESC');
                break;
            case 'release_date':
                $qbIds->orderBy('b.creation_date','DESC');
                break;
            case 'title':
                $qbIds->orderBy('b.title','ASC');
                break;
            case 'username':
                $qbIds
                    ->join('b.user','u')
                    ->orderBy('u.username','ASC');
                break;
        }

        if(!is_null($options['ids']) && count($options['ids']) > 0){
            $qbIds->where($qbIds->expr()->in('b.id',$options['ids']));
        }

        $query = $qbIds->getQuery();

        $resultCache_id = Board::RESULT_CACHE_COLLECTION_PREFIX.md5($query->getSQL());
        $resultCache_ttl = Board::RESULT_CACHE_COLLECTION_TTL;

        $query
            ->useQueryCache(true)
            ->setResultCacheLifetime($resultCache_ttl)
            ->setResultCacheId($resultCache_id);

        $pagination = null;
        if (!is_null($options['pagination'])) {
            $arrIds = array_map(function ($b) {
                return $b['id'];
            }, $qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else{
            $queryIds = $qbIds->getQuery();
            if (!is_null($options['max_results'])) {
                $queryIds->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($b) {
                return $b['id'];
            }, $queryIds->getArrayResult());
        }

        $results = array();

        if(count($ids) > 0) {
            foreach ($ids as $id) {
                $qb = $this->em->createQueryBuilder();

                $qb
                    ->select('board')
                    ->addSelect('user')
                    ->from('ATMBoardBundle:Board','board')
                    ->join('board.user','user')
                    ->where($qb->expr()->eq('board.id', $id));

                $query = $qb->getQuery();
                $query
                    ->useQueryCache(true)
                    ->setResultCacheLifetime(Board::RESULT_CACHE_ITEM_TTL)
                    ->setResultCacheId(Board::RESULT_CACHE_ITEM_PREFIX.$id);

                $board = $query->getArrayResult();
                if(isset($board[0])){
                    $results[] = $board[0];
                }
            }
        }

        return array(
            'results' => $results,
            'pagination' => $pagination
        );
    }
}