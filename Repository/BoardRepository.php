<?php

namespace ATM\BoardBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BoardRepository extends EntityRepository{

    public function getUserBoard($userId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('board')
            ->from('ATMBoardBundle:Board','board')
            ->join('board.user','u','WITH',$qb->expr()->eq('u.id',$userId));

        $board = $qb->getQuery()->getArrayResult();

        return isset($board[0]) ? $board[0] : null;
    }

    public function getBoard($usernameCanonical){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('board')
            ->addSelect('user')
            ->from('ATMBoardBundle:Board','board')
            ->join('board.user','user','WITH',$qb->expr()->eq('user.usernameCanonical',$qb->expr()->literal($usernameCanonical)));

        $board = $qb->getQuery()->getArrayResult();

        return isset($board[0]) ? $board[0] : null;
    }

    public function getBoardPostsDates($boardId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb1 = $this->getEntityManager()->createQueryBuilder();

        $qb1
            ->select('COUNT(p1.id)')
            ->from('ATMBoardBundle:Post','p1')
            ->join('p1.board','b1','WITH',$qb->expr()->eq('b1.id',$boardId))
            ->where($qb1->expr()->eq('DATE(p1.creation_date)','DATE(p.creation_date)'));


        $qb
            ->select('DATE(p.creation_date) as creationDate')
            ->addSelect('('.$qb1->getQuery()->getDQL().') as totalPosts')
            ->from('ATMBoardBundle:Post','p')
            ->join('p.board','b','WITH',$qb->expr()->eq('b.id',$boardId))
            ->groupBy('creationDate');

        return $qb->getQuery()->getArrayResult();
    }
}