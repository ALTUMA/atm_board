<?php

namespace ATM\BoardBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ACLRepository extends EntityRepository{

    public function userHasAccess($boardId,$userId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('acl.id')
            ->from('ATMBoardBundle:ACL','acl')
            ->join('acl.board','b','WITH',$qb->expr()->eq('b.id',$boardId))
            ->join('acl.user','u','WITH',$qb->expr()->eq('u.id',$userId));

        return !is_null($qb->getQuery()->getOneOrNullResult()) ? true : false;
    }
}