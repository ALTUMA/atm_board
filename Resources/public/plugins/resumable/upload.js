function formatBytes(bytes, decimals) {
    if(bytes == 0) return '0 Byte';
    var k = 1000; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var dm = decimals;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function getFileExtension(fileName){
    return fileName.split('.').pop();
}

var r = new Resumable({
    target: upload_url,
    chunkSize: 1*1024*1024,
    simultaneousUploads: 1,
    testChunks: false,
    throttleProgressCallbacks: 1,
    withCredentials: true,
    permanentErrors:[400, 404, 415, 500, 501, 503]
});
// Resumable.js isn't supported, fall back on a different method
if(!r.support) {
    document.getElementById('error-log').innerHTML = 'Your browser, unfortunately, is not supported by Resumable.js. The library requires support for <a href="http://www.w3.org/TR/FileAPI/">the HTML5 File API</a> along with <a href="http://www.w3.org/TR/FileAPI/#normalization-of-params">file slicing</a>.';
} else {
    // Show a place for dropping/selecting files
    $('.resumable-drop').show();
    r.assignBrowse(document.getElementById('resumable-browse'));
    r.assignDrop(document.getElementById('resumable-drop'));

    // Handle file add event
    r.on('fileAdded', function(file){
        var fileExt = getFileExtension(file.fileName).toLowerCase();
        var indexOf = allowedFileExtensions.indexOf(fileExt);
        var extAllowed = (indexOf != -1);
        if (extAllowed){
            $("#progressbar").show();
            document.getElementById('error-log').innerHTML = '';
            r.upload();
        } else {
            r.cancel();
            $("#progressbar").hide();
            document.getElementById('error-log').innerHTML = '<b>ERROR:</b> File type not allowed.';
            $("#error-log").show();
        }
    });
    r.on('pause', function(){

    });
    r.on('complete', function(){
        $('#hdnIsVideo').val(1);
        $('#hdnIsImage').val(0);
    });
    r.on('fileSuccess', function(file, message){
        document.getElementById('error-log').innerHTML = '<b>File "' + file.fileName + '" uploaded successfully!</b>';
        $.ajax({
            data: {
                'file': file.fileName
            },
            async: false,
            type: 'POST',
            url: fix_permissions_url,
            success: function(success){
                $('#resumable-drop').remove();
                $('#videoFilename').val(success);
                $('#error-log').hide();
            }
        });
    });
    r.on('error', function(message, file){
        document.getElementById('error-log').innerHTML = 'Error: ' + message;
    });
    r.on('fileError', function(file, message){
        document.getElementById('error-log').innerHTML = 'Error: ' + message;
    });
    r.on('fileProgress', function(file){
        $("#progressbar > span").css({width: Math.floor(r.progress()*100) + '%'}).find('span').text(Math.floor(r.progress()*100) + '%');
    });
    r.on('cancel', function(){

    });
    r.on('uploadStart', function(){
        $('#fsChoseMedia').remove();
    });
}