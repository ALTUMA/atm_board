<?php

namespace ATM\BoardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use ATM\BoardBundle\Entity\Board;

class BoardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Name',
                    'autocomplete' => 'off'
                )
            ))
            ->add('description',TextareaType::class,array('required'=>false,'attr'=> array('placeholder'=>'Description')))
            ->add('visibility',ChoiceType::class,array(
                'choices' => array(
                    'Public' => Board::VISIBILITY_PUBLIC,
                    'Private' => Board::VISIBILITY_PRIVATE,
                    'Sold' => Board::VISIBILITY_SOLD
                )
            ))
            ->add('price',MoneyType::class,array(
                'required' => false,
                'currency' => null,
                'attr' => array(
                    'placeholder' => 'Price'
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Board::class,
        ));
    }

    public function getName()
    {
        return 'atmboard_bundle_board_type';
    }
}