<?php


namespace ATM\BoardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller{

    /**
     * @Route("/{page}", name="atm_board_admin_index", defaults={"page":1})
     */
    public function indexAction($page){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_board_config');
        $paginator = $this->get('knp_paginator');

        $qbIds = $em->createQueryBuilder();
        $qbIds
            ->select('board.id')
            ->from('ATMBoardBundle:Board','board')
            ->orderBy('board.id','DESC');

        $boardIds = array_map(function($b){
            return $b['id'];
        },$qbIds->getQuery()->getArrayResult());

        $boards = $pagination = null;
        if(count($boardIds) > 0){
            $pagination = $paginator->paginate(
                $boardIds,
                $page,
                25
            );

            $ids = $pagination->getItems();

            $qb = $em->createQueryBuilder();
            $qb
                ->select('board')
                ->addSelect('user')
                ->from('ATMBoardBundle:Board','board')
                ->leftJoin('board.user','user')
                ->where($qb->expr()->in('board.id',$ids));

            $boards = $qb->getQuery()->getArrayResult();
        }



        return $this->render('ATMBoardBundle:Admin:index.html.twig',array(
            'boards' => $boards,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/search/user/{username}", name="atm_board_admin_search_users", options={"expose"=true})
     */
    public function searchUserAction($username){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_board_config');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('user')
            ->addSelect('board')
            ->from($config['user'],'user')
            ->leftJoin('user.boards','board')
            ->where($qb->expr()->eq('user.username',$qb->expr()->literal($username)));

        $user = $qb->getQuery()->getOneOrNullResult();

        if(!is_null($user)){
            return $this->render('ATMBoardBundle:Admin:item/user_row.html.twig',array(
                'user' => $user
            ));
        }else{
            return new Response('ko');
        }

    }
}