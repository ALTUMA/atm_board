<?php

namespace ATM\BoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_board_post")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection")
 * })
 */
class Post{

    const RESULT_CACHE_ITEM_PREFIX = 'atm_board_post';
    const RESULT_CACHE_ITEM_TTL = 36000000;
    const RESULT_CACHE_COLLECTION_PREFIX = 'atm_board_posts';
    const RESULT_CACHE_COLLECTION_TTL = 36000000;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creation_date;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="post", cascade={"remove"})
     */
    protected $images;

    /**
     * @ORM\OneToOne(targetEntity="Video", mappedBy="post", cascade={"remove"})
     */
    protected $video;

    /**
     * @ORM\Column(name="description", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Board", inversedBy="posts")
     * @ORM\JoinColumn(name="board_id", referencedColumnName="id")
     **/
    protected $board;

    private $post_to_link;

    public function __construct(){
        $this->creation_date = new DateTime();
        $this->isEncoded = true;
        $this->images = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function addImage($image)
    {
        $this->images->add($image);
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setVideo($video)
    {
        $this->video = $video;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = strip_tags($description,'<span><br>');
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function setBoard($board)
    {
        $this->board = $board;
    }

    public function getPostToLink()
    {
        return $this->post_to_link;
    }

    public function setPostToLink($post_to_link)
    {
        $this->post_to_link = $post_to_link;
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return $this::RESULT_CACHE_ITEM_PREFIX.$this->getId();
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX.$this->getBoard()->getId();
    }
}