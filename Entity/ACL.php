<?php

namespace ATM\BoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\BoardBundle\Repository\ACLRepository")
 * @ORM\Table(name="atm_board_acl")
 */
class ACL{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\ManyToOne(targetEntity="Board")
     * @ORM\JoinColumn(name="board_id", referencedColumnName="id")
     **/
    protected $board;

    protected $user;

    public function __construct()
    {
        $this->creation_date = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function setBoard($board)
    {
        $this->board = $board;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}