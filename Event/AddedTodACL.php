<?php

namespace ATM\BoardBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AddedTodACL extends Event{

    const NAME = 'atm_board_added_acl.event';

    protected $board;
    protected $user;

    public function __construct($board,$user)
    {
        $this->board = $board;
        $this->user = $user;
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function setBoard($board)
    {
        $this->board = $board;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}