<?php

namespace ATM\BoardBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class RemovedFromACL extends Event{

    const NAME = 'atm_board_deleted_acl.event';

    protected $boardId;
    protected $userId;

    public function __construct($boardId,$userId)
    {
        $this->boardId = $boardId;
        $this->userId = $userId;
    }

    public function getBoardId()
    {
        return $this->boardId;
    }

    public function setBoardId($boardId)
    {
        $this->boardId = $boardId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}