<?php

namespace ATM\BoardBundle\Queues\Video;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use Doctrine\ORM\EntityManagerInterface;
use Qencode\Exceptions\QencodeApiException;
use Qencode\Exceptions\QencodeException;
use Qencode\QencodeApiClient;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'atm_board_encode_video:execute';
    private $em;
    private $rootDir;
    private $config;
    private $cdn;

    public function __construct(EntityManagerInterface $em, $cdn,$kernel_rootdir,$atm_board_config)
    {
        parent::__construct();
        $this->em = $em;
        $this->rootDir = $kernel_rootdir;
        $this->config = $atm_board_config;
        $this->cdn = $cdn;
    }

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        //return true;
        try{
            $body = json_decode($msg->body,true);

            $videoFilename = $body['videoFilename'];
            $postId = $body['post_id'];
            $user_id = $body['user_id'];
            $user = $this->em->getRepository($this->config['user'])->findOneById($user_id);


            $userFolderName = $user->getUsernameCanonical();
            $userDir = $this->rootDir.'/../web/'.$this->config['media_folder'].'/'.$userFolderName.'/videos';

            $fileName = md5(uniqid());
            $fileTokens = explode('.',$videoFilename);
            $destVideoName = $fileName.'.'.$fileTokens[1];

            if(strpos($videoFilename,' ') !== FALSE){
                $oldVideoFilename = $videoFilename;
                $videoFilename = str_replace(' ','',$videoFilename);
                rename($userDir.'/output/'.$oldVideoFilename,$userDir.'/output/'.$videoFilename);
            }

            copy($userDir.'/output/'.$videoFilename,$userDir.'/'.$destVideoName);
            unlink($userDir.'/output/'.$videoFilename);

            if(!is_dir($this->rootDir.'/../web/uploads/qencode')){
                mkdir($this->rootDir.'/../web/uploads/qencode');
            }

            if(!is_dir($this->rootDir.'/../web/uploads/qencode/1080')){
                mkdir($this->rootDir.'/../web/uploads/qencode/1080');
            }

            if(!is_dir($this->rootDir.'/../web/uploads/qencode/image')){
                mkdir($this->rootDir.'/../web/uploads/qencode/image');
            }

            $videoUrl = $this->cdn->getCDNResource(array(
                'media_path' => '/'.$this->config['media_folder'].'/'.$userFolderName.'/videos/'.$destVideoName,
                //'media_path' => '/media/promotools/advertisements/qencode/9f9d76915aa4c11aba6fd0a9f4a1b068.mov',
                'tokenize' => true,
                'expiration_ttl' => 3600
            ));

            $apiKey = $this->config['encoder_api_key'];
            $transcodingProfileId = $this->config['encoder_profile_id'];
            $transferMethodId = $this->config['encoder_transfer_method_id'];
        }catch(\Exception $e){
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }
            return false;
        }

        $q = new QencodeApiClient($apiKey);
        try {

            $task = $q->createTask();  // /v1/create_task
            dump("Created task: ".$task->getTaskToken());
            $task->start_time = '';
            $task->duration = '';

            $task->start($transcodingProfileId, $videoUrl, $transferMethodId);

            do {
                sleep(5);
                $response = $task->getStatus();
                if (is_array($response) and array_key_exists('percent', $response)) {
                    dump("Completed: {$response['percent']}%");
                }
            } while ($response['status'] != 'completed');



            foreach ($response['videos'] as $video) {
                $encodedVideoPath = $this->rootDir.'/../web/uploads/qencode/1080/'.basename($video['storage']['path']);
                copy($encodedVideoPath,$userDir.'/'.$destVideoName);
                unlink($encodedVideoPath);

                $imageName = str_replace('.mp4','.jpg',basename($video['storage']['path']));
                $imagePath = $this->rootDir.'/../web/uploads/qencode/image/'.$imageName;
                copy($imagePath,$userDir.'/'.$imageName);
                unlink($imagePath);

                $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
                if($post){
                    $video = $post->getVideo();
                    $video->setVideo($this->config['media_folder'].'/'.$userFolderName.'/videos/'.$destVideoName);
                    $video->setThumbnail($this->config['media_folder'].'/'.$userFolderName.'/videos/'.$imageName);
                    $video->setIsEncoded(true);

                    $this->em->persist($video);
                    $this->em->flush();
                }
            }

        }catch(QencodeClientException $e){
            dump('Qencode Client Exception: ' . $e->getCode() . ' ' . $e->getMessage());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }

        }catch(QencodeApiException $e){
            // API response status code was not successful
            dump('Qencode API Exception: ' . $e->getCode() . ' ' . $e->getMessage());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }

        }catch(QencodeException $e){
            dump('Qencode Exception: ' . $e->getMessage());
            dump($q->getLastResponseRaw());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }
        }catch(\Exception $e){
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }
        }

    }
}

