# Installation #

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/boardbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\BoardBundle\ATMBoardBundle(),
    ];
}
```

# Configuration sample #
``` yml
# app/config/config.yml
  
atm_board:
    media_folder: Media folder.
    user: Your user namespace.
    image_width: width of the post image.
    image_height: height of the post image.
    watermark_image_small: small watermark path.
    watermark_image_medium: medium watermark path.
    watermark_image_big: big watermark path.
    encoder_api_key: encoder api key from qencode.
    encoder_profile_id: encoder profile id from qencode.
    encoder_transfer_method_id: encoder profile id from qencode.
    not_access_redirect_route: Redirect route when a user has no access to the board.
    after_request_access_redirect_route: Redirect route after a request has sent to a user.
    added_to_acl_redirect_route: Redirect route after a user is added to the acl.
    removed_to_acl_redirect_route: Redirect route after a user is removed to the acl.
```

# Routing #
Append to the main routing file:
```yml
# app/config/routing.yml

atm_board:
    resource: "@ATMBoardBundle/Controller/BoardController.php"
    type:     annotation
    prefix:   /

atm_board_admin:
    resource: "@ATMBoardBundle/Controller/AdminController.php"
    type:     annotation
    prefix:   /
```
# Usage #
First create a board using the following route:
```twig
    {{ path('atm_board_create')
```

Once your board is created you can add post images, videos and texts. For videos you will need to
start the rabbitMQ queue in your console:

    - atm_board_encode_video:execute

# The ACL
The board system also allows to give access to users that the owner of the board wants. First of all
the user that wants access to your board has to send you a request using this route:
```twig
    {{ path('atm_board_request_access') }}
```
The action of this route will throw the following event:
```yml
    atm_board_request_access.event
```
In that event you can customize the way that the owner of the board will get the link to add the
user that wants to check the board. That's the route for adding a user to a board's acl:
```twig
    {{ path('atm_board_add_user_acl',{ 'boardId':board.id, 'userId':user.id }) }}
```
After adding the user to the board's acl the following event is thrown, with the board id and the user id added 
to the board acl as parameters:
```yml
    atm_board_added_acl.event
```

If the owner of the board wants to remove a user from it's board that's the route to do that:
```twig
    {{ path('atm_board_remove_user_acl',{ 'boardId':board.id, 'userId':user.id }) }}
```
This action throws the following event with the board id and the user id removed from the board acl as parameters 
```yml
    atm_board_deleted_acl.event
```

# The Admin
The bundle has it's own admin panel to see all the users with a link to their boards, that's the route to the admin
panel:
```twig
    {{ path('atm_board_admin_index')
```