<?php

namespace ATM\BoardBundle\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use ATM\BoardBundle\Entity\Post;
use ATM\BoardBundle\Entity\Video;

class VideoEncoded{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $e = $args->getEntity();

        if (!$e instanceof Video) {
            return;
        }

        $this->em->getConfiguration()->getResultCacheImpl()->delete(Post::RESULT_CACHE_ITEM_PREFIX.$e->getPost()->getId());
    }
}